# DL4J動作サンプル

## 環境準備

必要なツール

* Java 8
* Git
* EclipseなどのIDE

## ソースコードを入手
git clone https://minebreaker_tf@bitbucket.org/minebreaker_tf/dl4jtest.git

## IDEで開く
Java用のビルドツールGradleを使っています.  
Eclipse等で開く場合、Gradleプロジェクトとしてインポートしてください.  
