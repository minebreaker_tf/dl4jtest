package dl4jtest;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.collection.CollectionRecordReader;
import org.datavec.api.writable.DoubleWritable;
import org.datavec.api.writable.IntWritable;
import org.datavec.api.writable.Writable;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.distribution.GaussianDistribution;
import org.deeplearning4j.nn.conf.distribution.NormalDistribution;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.DoubleStream;

import static java.util.stream.Collectors.toList;

public final class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static long seed = 123;
    private static Random random = new Random(seed);
    private static int iterations = 1;
    private static int batch = 100;
    private static int labelInd = 2;
    private static int labelNum = 2;

    public static void main(String[] args) throws IOException {

        // GPU用の設定
//        DataTypeUtil.setDTypeForContext(DataBuffer.Type.HALF);
//        CudaEnvironment.getInstance().getConfiguration()
//                       .setMaximumDeviceCache(6L * 1024L * 1024L * 1024L);

        // 三層の順伝播型ネットワーク
        MultiLayerNetwork network = new MultiLayerNetwork(
                new NeuralNetConfiguration.Builder()
                        .seed(seed)  // 乱数初期化のシード値
                        .iterations(iterations)
                        .miniBatch(false)  // ミニバッチは使用しない
                        .weightInit(WeightInit.DISTRIBUTION)  // 重みの初期化方法
                        .dist(new NormalDistribution(0.0, 0.01))  // デフォルトは標準偏差0.01、各層ではガウス分布を使用
                        .activation(Activation.RELU)  // 活性化関数に正規化直線関数
                        .updater(Updater.NESTEROVS).momentum(0.9)  // 重みの更新にモーメンタムを使用
                        .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)  // 確率的勾配降下法
                        .learningRate(0.01)  // 重みの学習率
                        .biasLearningRate(0.02)  // バイアスの学習率
                        .regularization(true).l2(0.0005)  // 正規化
                        .list()

                        // 層の設定
                        .layer(0, new DenseLayer.Builder()
                                .name("l1")
                                .nIn(2)  // [ 辺の長さ, 面積 ]
                                .nOut(8)  // 出力: 次の層の入力と等しくなる
                                .dist(new GaussianDistribution(0, 0.005)) // パラメーターの初期化: ガウス分布
                                .build())
                        .layer(1, new DenseLayer.Builder()
                                .name("l2")
                                .nIn(8)
                                .nOut(32)
                                .dist(new GaussianDistribution(0, 0.005))
                                .build())
                        .layer(2, new OutputLayer.Builder()
                                .name("out")
                                .nOut(2)  // 三角形 / 正方形
                                .activation(Activation.SOFTMAX)  // 出力層はソフトマックス関数を使う
                                .build())

                        .backprop(true)  // 誤差逆伝播法
                        .pretrain(false)  // 事前学習は行わない
                        .build());
        network.init();
        network.setListeners(new ScoreIterationListener(1));

        for (int i = 0; i < 100; i++) {
            RecordReader trainRecordReader = new CollectionRecordReader(getData(batch));
            DataSetIterator trainIter = new RecordReaderDataSetIterator(trainRecordReader, batch, labelInd, labelNum);
            DataSet trainData = trainIter.next();

            // 実際の訓練
            network.fit(trainData);

            RecordReader testRecordReader = new CollectionRecordReader(getData(100));
            DataSetIterator testIter = new RecordReaderDataSetIterator(testRecordReader, 100, labelInd, labelNum);
            DataSet testData = testIter.next();

            // 性能を評価する
            Evaluation eval = new Evaluation();
            eval.eval(testData.getLabels(), network.output(testData.getFeatures()));
            logger.info("{}", eval.stats());
        }
    }

    /**
     * 正方形と三角形の、辺の長さ-面積のデータセットを作る.
     */
    private static List<List<Writable>> getData(int size) {
        return DoubleStream.generate(random::nextDouble)
                           .mapToObj(n -> n >= 0.5 ? triangle() : rectangle())
                           .limit(size)
                           .collect(toList());
    }

    /**
     * 正三角形のデータセット
     */
    private static List<Writable> triangle() {
        double seed = largeRandom();
        return Arrays.asList(
                new DoubleWritable(seed),
                new DoubleWritable(seed * seed * Math.sqrt(3) / 4),
                new IntWritable(0)  // ラベル
        );
    }

    /**
     * 正方形のデータセット
     */
    private static List<Writable> rectangle() {
        double seed = largeRandom();
        return Arrays.asList(
                new DoubleWritable(seed),
                new DoubleWritable(seed * seed),
                new IntWritable(1)  // ラベル
        );
    }

    /**
     * ランダムなdoubleを生成する。
     * パラメーターに下限を設定することで、差の小さいデータが作られることを防ぐ.
     * (辺の長さが小さいほど、面積の差も小さくなるので)
     */
    private static double largeRandom() {
        double r = random.nextDouble();
        return r >= 0.3 ? r : largeRandom();
    }

}
